function filter(elements,callBack) {
    if(elements == undefined || Array.isArray(elements)==false || callBack == undefined || callBack.constructor != Function){
        return [];
    } else {
        let filteredArray = [];
        for(let index=0; index<elements.length; index++){
            let isTrue = callBack(elements[index],index,elements);
            if(isTrue === true){
                filteredArray.push(elements[index]);
            }
        }
        return filteredArray;
    }
}

module.exports = filter;
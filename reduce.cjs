function reduce(elements,callBack,startingValue){
    if(elements == undefined || Array.isArray(elements) == false || callBack == undefined || callBack.constructor != Function){
        return 0;
    } else if(elements.length == 0 && startingValue != undefined){
        return startingValue;
    } else{
        let isStartingDefined = true;
        if(startingValue == undefined){
            isStartingDefined = false;
            startingValue = elements[0];
        }
        let result = undefined;
        for(let index=0; index<elements.length; index++){
            if(isStartingDefined == false){
                isStartingDefined = true;
                continue;
            }
            result = callBack(startingValue,elements[index],index,elements);
            startingValue = result;
        }
        return result;
    }
}

module.exports = reduce;
function flatten(elements,depth = 1){
    let resultArray = [];
    if(Array.isArray(elements) == false || elements == undefined){
        return [];
    }
    function flattenArray(elements,depth){
        for(let arrayElement of elements){
            if(arrayElement === undefined){
                continue;
            } else if(Array.isArray(arrayElement) === false || depth <= 0){
                resultArray.push(arrayElement);
            } else {
                flattenArray(arrayElement,depth-1);
            }
        }
    }
    flattenArray(elements,depth);
    return resultArray;
}

module.exports = flatten;
function each(elements,callBack){
    if(elements == undefined || Array.isArray(elements)==false || callBack == undefined || callBack.constructor != Function){
        return;
    } else { 
        for(let index=0; index<elements.length; index++){
            callBack(elements[index],index);
        }
    }
}

module.exports = each;

function find(elements,callBack){
    if(elements == undefined || Array.isArray(elements) == false || callBack == undefined || callBack.constructor != Function){
        return undefined;
    }
    for(let index = 0; index<elements.length; index++){
        let isTrue = callBack(elements[index]);
        if(isTrue){
            return elements[index];
        }
    }
    return undefined;
}

module.exports = find;
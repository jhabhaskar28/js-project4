function map(elements,callBack){
    if(elements == undefined || Array.isArray(elements)==false || elements.length == 0){
        return [];
    } else if (callBack == undefined || typeof(callBack)!= 'function' || callBack.constructor != Function){
        return elements;
    } else {
        let resultArray = [];
        for(let index = 0; index<elements.length ; index++){
            let mappedElement = callBack(elements[index],index,elements);
            resultArray.push(mappedElement);
        }
        return resultArray;
    }
}
module.exports = map;